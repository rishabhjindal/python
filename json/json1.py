import json
import urllib.request, urllib.parse, urllib.error
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter location: ')
if len(url) < 1 : url = "http://py4e-data.dr-chuck.net/comments_192322.json"
print('Retrieving', url)

json_str = urllib.request.urlopen(url, context=ctx).read()
stringlen = len(json_str)
print('Retrieved', stringlen, 'characters')

data = json.loads(json_str)
#print(data)
lst = data["comments"]
#print(lst)
sum  = 0
for info in lst:
    nun = int(info['count'])
    #print(nun)
    sum = sum + nun
print('Sum:', sum)
