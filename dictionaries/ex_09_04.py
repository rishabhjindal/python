name = input("Enter file:")
if len(name) < 1 : name = "mbox-short.txt"
try:
    fh = open(name)
except:
    print('No file exist')
    exit()
count = dict()
index = 0
for line in fh:
    each = line.split()
    #print(each)
    if 'From' in each:
        count[each[index+1]] = count.get(each[index+1], 0) + 1
#print(count)
maxcount = None
maxkey = None
for word, count in count.items():
    if maxcount is None or count > maxcount :
        maxcount = count
        maxkey = word

print(maxkey, maxcount)
