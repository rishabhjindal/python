import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter location: ')
if len(url) < 1 : url = "http://py4e-data.dr-chuck.net/comments_192321.xml"
print('Retrieving', url)

xml_txt = urllib.request.urlopen(url, context=ctx).read()
stringlen = len(xml_txt)
print('Retrieved', stringlen, 'characters')
#print(xml_txt)
tree = ET.fromstring(xml_txt)
#print(tree)
counts = tree.findall('comments/comment')
#print(counts)
print('Count:', len(counts))
sum  = 0
for item in counts:
    num = item.find('count').text
    #print(num)
    sum = sum + int(num)
print('Sum:', sum)
