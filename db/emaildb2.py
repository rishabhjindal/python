import sqlite3

conn = sqlite3.connect('emaildb2.sqlite')
cur = conn.cursor()

cur.execute('DROP TABLE IF EXISTS Counts')

cur.execute('''CREATE TABLE Counts (org TEXT, count INTEGER)''')

fname = input('Enter file name: ')
if (len(fname) < 1): fname = 'mbox.txt'
fh = open(fname)

insert_value = []
email_value = []
count_value = []

for line in fh:
    if not line.startswith('From: '): continue
    pieces = line.split()
    email = pieces[1]
    #print(email)
    emails = email.split('@')
    org = emails[1]
    #print(org)
    if org in email_value:
        index = email_value.index(org)
        #print(index)
        cur_count = count_value[index]
        new_count = int(cur_count) + 1
        count_value[index] = new_count
        insert_value[index] = (org, new_count)
    else:
        insert_value.append((org, 1))
        email_value.append(org)
        count_value.append(1)
    #conn.commit()

#print(insert_value)
#print(email_value)
#print(count_value)
cur.executemany('''INSERT INTO Counts (org, count) VALUES (?, ?)''', insert_value)
conn.commit()

# https://www.sqlite.org/lang_select.html
sqlstr = 'SELECT org, count FROM Counts ORDER BY count DESC'

for row in cur.execute(sqlstr):
    print(str(row[0]), row[1])

cur.close()
