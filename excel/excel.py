import openpyxl

def copyRange(startCol, startRow, endCol, endRow, sheet):
    rangeSelected = []
    #Loops through selected Rows
    for i in range(startRow,endRow + 1,1):
        #Appends the row to a RowSelected list
        rowSelected = []
        for j in range(startCol,endCol+1,1):
            rowSelected.append(sheet.cell(row = i, column = j).value)
        #Adds the RowSelected List and nests inside the rangeSelected
        rangeSelected.append(rowSelected)

    return rangeSelected

def pasteRange(startCol, startRow, endCol, endRow, sheetReceiving,copiedData):
    countRow = 0
    for i in range(startRow,endRow+1,1):
        countCol = 0
        for j in range(startCol,endCol+1,1):
            sheetReceiving.cell(row = i, column = j).value = copiedData[countRow][countCol]
            #print(sheetReceiving.cell(row = i, column = j).value)
            countCol += 1
        countRow += 1

print("Processing...")
wb2 = openpyxl.load_workbook('test.xlsx')
#print(wb2.sheetnames)
sheet = wb2.get_sheet_by_name('Sheet1')
#print(sheet)
#cell_range = sheet['A1':'A1']
#print(cell_range)
selectedRange = copyRange(1,1,1,1,sheet)

wb = openpyxl.Workbook()
dest_filename = 'test2.xlsx'
ws1 = wb.active
ws1.title = "data"
wb.save(filename = dest_filename)

template = openpyxl.load_workbook("test2.xlsx")
temp_sheet = template.get_sheet_by_name("data")

pasteRange(1,1,1,1,temp_sheet,selectedRange)

template.save(filename = dest_filename)

print("Range copied and pasted!")
