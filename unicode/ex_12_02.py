import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter URL- ')
if len(url) < 1 : url = "http://py4e-data.dr-chuck.net/known_by_Airidas.html"

count = input('Enter count: ')
position = input('Enter position: ')

curr_count = 0
total_count = int(count)
index = int(position) - 1

while curr_count < total_count :
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, "html.parser")
    tags = soup('a')
    url = tags[index].get('href', None)
    name = soup.findAll("a")[index].string
    print('Retrieving:', url)
    print('Name:', name)
    curr_count = curr_count + 1
