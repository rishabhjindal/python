from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter - ')
if len(url) < 1 : url = "http://py4e-data.dr-chuck.net/comments_192319.html"
html = urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, "html.parser")

tags = soup('span')
sum  = 0
count = 0
for tag in tags :
    #print('TAG:', tag)
    volume = soup.findAll("span", {"class": "comments"})[count].string
    #print(volume)
    count = count + 1
    sum = int(volume) + sum
print(sum)
