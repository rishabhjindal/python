import re

hand = open("regex_sum_192317.txt")
lst = list()
for line in hand:
    curr = re.findall('[0-9]+', line)
    lst = lst + curr

sum = 0
for tot in lst:
    sum = sum + int(tot)

print(sum)
