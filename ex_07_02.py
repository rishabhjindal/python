# Use the file name mbox-short.txt as the file name
fname = input("Enter file name: ")
fh = open(fname)
total = 0
count = 0
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    atpos = line.find('0')
    line = line.strip()
    line = line[atpos:]
    try:
        fline = float(line)
        total = total + fline
        count = count + 1
        #print(total)
        #print(count)
    except:
        print('Invalid number')
        continue
#print(total)
#print(count)
avg = total / count
favg = float("%.12f" % avg)
print('Average spam confidence:', favg)
